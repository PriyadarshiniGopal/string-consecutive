﻿using System;

namespace String_Consecutive
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Strings separated by -");
            String numbers = Console.ReadLine();
            String[] values = numbers.Split("-");
            int difference=0;
            if (Convert.ToInt32(values[0]) - Convert.ToInt32(values[1]) == 1)
                difference = 1;
            else if (Convert.ToInt32(values[0]) - Convert.ToInt32(values[1]) == -1)
                difference = -1;
            for(int i = 0; i < values.Length-1; i++)
            {
                if(Convert.ToInt32(values[0]) - Convert.ToInt32(values[1]) != difference || difference==0)
                {
                    Console.WriteLine("Numbers are not consecutive");
                    Environment.Exit(0);
                }
            }
            Console.WriteLine("Numbers are consecutive");
            Console.WriteLine(values[0]);
        }
    }
}
